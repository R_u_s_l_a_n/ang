import { NullTemplateVisitor } from '@angular/compiler';

export interface Dish{
    id: number;
    name: string;
    discription: string;
    picture: string;
    price: number;
    

}