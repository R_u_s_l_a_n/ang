import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DishComponent } from './components/dish/dish.component';
import { OrderComponent } from './components/order/order.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuPageComponent } from './components/pages/pages.component';
import { RoutinggModule } from './routing.module';
import { ChanelsComponent } from './components/pages/chanels/chanels.component';
import { AboutComponent } from './components/pages/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrderComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    ChanelsComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    RoutinggModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
