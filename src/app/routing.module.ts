import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router'; // загружаем
import { MenuPageComponent } from './components/pages/pages.component';
import { ChanelsComponent } from './components/pages/chanels/chanels.component';
import { AboutComponent } from './components/pages/about/about.component';


const routes: Route[] = [
  {
    path: '',
    redirectTo: 'menu',     // страница по умолчанию
    pathMatch: 'full'         // проверка пути
  },
  {
    path: 'menu',
    component: MenuPageComponent
  },
  {
    path: 'chanels',
    component: ChanelsComponent
  },
  {
    path: 'about',
    component: AboutComponent
  }
];


@NgModule({

  imports: [
    RouterModule.forRoot(routes),             // подключаем сразу
  ],
  exports: [RouterModule]       // передаем на уровень выше в пользование
})

export class RoutinggModule {


}
