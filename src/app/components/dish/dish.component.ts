import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Dish } from 'src/app/models/dish.model';

@Component({
    selector: 'app-dish',
    templateUrl: './dish.component.html',
    styleUrls: ['./dish.component.scss']
})

export class DishComponent {
    @Input() item: Dish;
    @Output() selectEvent: EventEmitter<number> = new EventEmitter<number>();
    @Output() dblEvent = new EventEmitter<Dish>();

    constructor() {}

    public onDishSelect(): void {
      this.selectEvent.emit(this.item.id);
    }

    public onDblClick(event: MouseEvent): void { //MouseEvent- тип события для мыши
      event.preventDefault(); //остановка стандартного действия браузера на действие
      event.stopPropagation();
      this.dblEvent.emit(this.item);
    }
  }
