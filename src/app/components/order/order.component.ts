import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { Dish } from 'src/app/models/dish.model';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit, OnDestroy { //подключаем хуки
    @Input() order: Dish[] = [];
    @Output() delOrder = new EventEmitter<number>();
    @Output() deleteEvent = new EventEmitter<Dish>();

    constructor() {}

    ngOnInit() {
      console.log('Order component created');
    }
  ngOnDestroy() {
    console.log('Order component deleted');
  }
    public get summ(): number {
        let total = 0;
        this.order.forEach(item => {
          total += item.price;
        });
        return total;
    }


    public deleteItem(item: Dish): void {
      this.deleteEvent.emit(item);
    }
  }
