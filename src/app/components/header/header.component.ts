import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent  {


  constructor(private router: Router) {}   // router тут стал объектом Router (сингл тон)
                   // второй вариант роутинга. создали router с типом данных Router

  public redirectToChanels(): void {        // второй вариант роутинга
this.router.navigate(['chanels']);       // адрес куда редиректить ('chanels')
  }
public get isChanelsActive(): boolean {  // геттер. в зависимости от того находимся ли мы на странице chanels
const url = this.router.url;  // http://localhost:4200/chanels содержание переменной
return url.includes('chanels'); // проверка есть ли chanels  в строке или нет
}
  }
