import { Component } from '@angular/core';
import { Dish } from 'src/app/models/dish.model';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})

export class MenuPageComponent  {

  constructor() {}

  public dishes: Dish[] = [
    {
      id: 0,
      name: 'Шаурма',
      discription: '',
      picture: '../assets/LOL.jpg',
      price: 2.1
    },
    {
      id: 1,
      name: 'Пицца',
      discription: '',
      picture: '../assets/Pizza.jpg',
      price: 5.5
    },
    {
      id: 2,
      name: 'Картошка',
      discription: '',
      picture: '../assets/Potate.jpg',
      price: 9.1
    },
    {
      id: 3,
      name: 'Суши',
      discription: '',
      picture: '../assets/Sushi.jpg',
      price: 7.3
    },
    {
      id: 4,
      name: 'Салат',
      discription: '',
      picture: '../assets/Salt.jpg',
      price: 6.5
    }
  ];

  public order: Dish[] = [];

  public isShowModal: boolean = false;

  public modalData: Dish;

  public onSelect(id: number) {
    console.log('Item select: ', id);

    const item: Dish = this.dishes.find(dish => {
       return dish.id === id });
        this.order.push(item);
  }

  public get isHaveOrder(): boolean {
    return this.order.length > 0;
  }

  public showDicription(dish: Dish) {
    this.isShowModal = true;
    this.modalData = dish;
  }

  public onCloseModal() {
    this.isShowModal = false;
  }

  public onDeleteItem(item: Dish): void {
    const index = this.order.indexOf(item); // получаем номер эл-та в массиве
    this.order.splice(index, 1); // указываем сколько эл-тов надо удалить(1)
  }
  }
